package ir.ayrik.asms;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            getFragmentManager().beginTransaction().replace(android.R.id.content, new MainFragment()).commit();
    }

}