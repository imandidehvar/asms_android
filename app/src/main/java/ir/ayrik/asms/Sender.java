package ir.ayrik.asms;

import android.content.Context;
import android.os.Looper;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.BlockingQueue;

public class Sender extends Thread {

    Public final String msg = "Your OTP Code is: "

    public static BlockingQueue<OTPList.OTP> queue;
    private Context context;

    public Sender(Context context)
    {
        this.context = context;
    }

    @Override
    public void run() {
        while (true) {
            try {
                OTPList.OTP otp = queue.take();
                sendSMS(otp.getPhone(), msg + otp.getCode());
            } catch (InterruptedException e) {
                Log.d("Test=>", "Ex");
            }
        }
    }

    public void sendSMS(String phoneNo, String msg) {
        Log.d("Test=>", "initialized");
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}