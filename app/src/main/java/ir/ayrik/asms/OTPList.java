package ir.ayrik.asms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class OTPList{
    @SerializedName("response")
    @Expose
    private List<OTP> list;

    public List<OTP> getList() {
        return list;
    }

    public void setList(List<OTP> list) {
        this.list = list;
    }

    class OTP {
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("code")
        @Expose
        private String code;

        public OTP() {
        }

        public OTP(String phone, String code) {
            this.phone = phone;
            this.code = code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}


