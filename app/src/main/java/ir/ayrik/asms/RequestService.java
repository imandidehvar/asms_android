package ir.ayrik.asms;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestService extends Service {
    private static final int NOTIFICATION_ID = 1;
    private Handler handler;
    private int interval;
    public RequestService() {
    }

    private static Notification createNotification(Context context) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, MainApplication.CHANNEL)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_SERVICE);

        Intent intent = new Intent(context, MainActivity.class);

        builder.setContentTitle("Service Is Enabled")
                .setTicker("Getting List...");
        builder.setContentIntent(PendingIntent.getActivity(context, 0, intent, 0));
        return builder.build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //interval = intent.getIntExtra("interval", 5);
        interval = 5;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    MainApplication.CHANNEL,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
        startForeground(1, createNotification(this));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Restfull.getList(onCallback);
                handler.postDelayed(this,interval);
            }
        },interval);
    }

    private Callback<OTPList> onCallback = new Callback<OTPList>() {
        @Override
        public void onResponse(Call<OTPList> call, Response<OTPList> response) {
            if(response.body().getList().size() > 0)
                Toast.makeText(RequestService.this, "new data arrives", Toast.LENGTH_SHORT).show();
            for(int i = 0; i<response.body().getList().size(); i++)
            {
                try {
                    Sender.queue.put(response.body().getList().get(i));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<OTPList> call, Throwable t) {

        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
