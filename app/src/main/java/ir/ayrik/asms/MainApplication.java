package ir.ayrik.asms;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.LinkedBlockingQueue;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainApplication extends Application {
    public static final String CHANNEL = "default";
    private static final String API = "Your HostName";
    private static Retrofit instance;
    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder().setLenient().create();

        instance = new Retrofit.Builder()
                .baseUrl(API)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        Sender.queue = new LinkedBlockingQueue<OTPList.OTP>();

    }

    public static Retrofit apiProvider() {
        return instance;
    }
}
