package ir.ayrik.asms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

public class MainFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String KEY_STATUS = "status";
    public static final String KEY_DURATION = "duration";
    public static final String KEY_URL = "url";
    public static final String KEY_MESSAGE = "message";

    private SharedPreferences sharedPreferences;
    private Sender sender;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sender = new Sender(getActivity());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    private void startService() {
        Intent i = new Intent(getActivity(), RequestService.class);
        i.putExtra("interval", 5);
        ContextCompat.startForegroundService(getActivity(), i);
        sender.start();
    }

    private void stopService() {
        getActivity().stopService(new Intent(getActivity(), RequestService.class));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(KEY_STATUS)) {
            if(sharedPreferences.getBoolean(KEY_STATUS, false))
                startService();
            else
                stopService();
        }
    }



}
