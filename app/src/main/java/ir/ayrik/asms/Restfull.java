package ir.ayrik.asms;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;

public class Restfull {

    public static void getList(Callback<OTPList> onCallback)
    {
        Api OTP_API = MainApplication.apiProvider().create(Api.class);
        Call<OTPList> fetch_api = OTP_API.fetchList();
        fetch_api.enqueue(onCallback);
    }

    private interface Api
    {
        @GET("ASMS/fetchOTPList")
        Call<OTPList> fetchList();
    }
}
